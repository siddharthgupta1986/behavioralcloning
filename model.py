import tensorflow as tf
import matplotlib.image as mpimg
import csv, cv2
import numpy as np
import os
from os.path import join
import json
tf.python.control_flow_ops = tf


center_image_file_paths = []
left_image_file_paths = []
right_image_file_paths = []
steering_angles = []


training_data_dir = 'data'
with open(join(training_data_dir, 'driving_log.csv'), 'rt') as csv_file:
    reader = csv.reader(csv_file)
    for row in reader:
        center_image_file_paths.append(join(training_data_dir, row[0].strip()))
        left_image_file_paths.append(join(training_data_dir, row[1].strip()))
        right_image_file_paths.append(join(training_data_dir, row[2].strip()))
        steering_angles.append(row[3])

steering_angles = np.array(steering_angles).astype(float)

X_train = center_image_file_paths
y_train = steering_angles

print('training data size', len(X_train))


# shuffle the training data
from sklearn.utils import shuffle
X_train, y_train = shuffle(X_train, y_train)


# import relevant classes from keras library
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Flatten,Lambda
from keras.layers.convolutional import Convolution2D
from keras.layers.pooling import MaxPooling2D
from keras.layers.core import Dropout
from keras.optimizers import Adam


'''
 Returns a keras sequential model that is based on the nvidia's paper
 path to paper:
    https://images.nvidia.com/content/tegra/automotive/images/2016/solutions/pdf/end-to-end-dl-using-px.pdf
'''
def get_model():
    model = Sequential()

    model.add(Lambda(lambda x: x / 127.5 - 1.0, input_shape=(90, 320, 1)))

    model.add(Convolution2D(24, 5, 5, border_mode='same', subsample=(2, 2), activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(1, 1)))

    model.add(Convolution2D(36, 5, 5, border_mode='same', subsample=(2, 2), activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(1, 1)))

    model.add(Convolution2D(48, 5, 5, border_mode='same', subsample=(2, 2), activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(1, 1)))

    model.add(Convolution2D(64, 3, 3, border_mode='same', subsample=(1, 1), activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(1, 1)))

    model.add(Convolution2D(64, 3, 3, border_mode='same', subsample=(1, 1), activation='relu'))
    model.add(MaxPooling2D(pool_size=(3, 3), strides=(1, 1)))

    model.add(Flatten())

    model.add(Dense(1164, activation='relu'))
    model.add(Dropout(0.2))

    model.add(Dense(100, activation='relu'))
    model.add(Dropout(0.2))

    model.add(Dense(50, activation='relu'))

    model.add(Dense(10, activation='relu'))

    model.add(Dense(1))

    model.summary()
    return model


# method to convert an array of images to grayscale
def convert_to_grayscale(image_data):
    image_data_grayscale = np.array([cv2.cvtColor(x, cv2.COLOR_RGB2GRAY) for x in image_data])
    return image_data_grayscale.reshape(len(image_data_grayscale), 90, 320, 1)

batch_size = 32

def image_generator():
    while 1:
        for i in range(int(len(X_train)/batch_size)):
            image_file_paths = X_train[i * batch_size:(i + 1) * batch_size]
            image_labels = np.array(y_train[i * batch_size:(i + 1) * batch_size])
            
            images = []
            for path in image_file_paths:
                image = mpimg.imread(path).astype(np.float32)
                # Resize to only relevant section of the image
                image = image[50:140, :]
                images.append(image)
            
            yield convert_to_grayscale(images), image_labels

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)

model = get_model()
model.compile(optimizer=Adam(0.0001), loss='mse')

model.fit_generator(
    image_generator(),
    samples_per_epoch=(int(len(X_train) / batch_size)) * batch_size,
    nb_epoch=10,
    verbose=1, 
    callbacks=[], 
    validation_data=None, 
    class_weight=None)


def delete_file_if_exists(filename):
    if os.path.isfile(filename):
        try:
            os.remove(filename)
        except OSError:
            pass

model_def_file = 'model.json'
model_weights_file = 'model.h5'

delete_file_if_exists(model_def_file)
delete_file_if_exists(model_weights_file)

json_string = model.to_json()
with open(model_def_file, 'w') as outfile:
    json.dump(json_string, outfile)

model.save_weights(model_weights_file)

print("model and weights are saved to files:", model_def_file, model_weights_file)
